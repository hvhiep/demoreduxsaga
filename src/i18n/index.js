import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import vi from './vi.json';
import en from './en.json';

// Set the key-value pairs for the different languages you want to support.
i18n.translations = {
    en, vi
}
// Set the locale once at the beginning of your app.
/**
 *TODO:DEVELOPER: 
 "locale": "en-US",
  "locales": Array [
    "en-US",
    "vi-US",
    "en",
    "vi-VN",
  ],
  "region": "US",
  "timezone": "Asia/Ho_Chi_Minh",
  => can change language width: i18n.locale = 'vi-VN or en-US
 */
i18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;

export { en, vi };
export default i18n;