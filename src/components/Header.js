import React, { useState } from 'react';
import { TextInput, Dimensions } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Form, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Searchbar } from 'react-native-paper';

const MyHeader = () => {

    const [value, setValue] = useState('');
    const [isShowSearch, setShowSearch] = useState(false);
    const onSearch = () => {setShowSearch(true)};
    const onComplete = () => {setShowSearch(false)};

    
    const onChangeText  = (value)=> {
        console.log(`${value}`);
    }

    return (
        <Header searchBar={true}>
            <Title>Header</Title>
            <Text>Text nativebase</Text>
            <Left>
                <TouchableOpacity onPress={onSearch}>
                    <Text>Search</Text>
                </TouchableOpacity>
            </Left>
            {/* <Body>
                
            </Body> */}
            {isShowSearch && <Searchbar onChangeText={onChangeText} onIconPress={onComplete} style={{width: '100%', height:' 100%'}} />}
            <Right>
            </Right>
        </Header>
    )
}

export default MyHeader;