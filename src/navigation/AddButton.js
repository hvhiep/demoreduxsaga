import React from "react";
import { FontAwesome5, Feather } from "@expo/vector-icons";
import { View, StyleSheet, TouchableHighlight, Animated } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";


const AddButton = ({onPress = ()=> {} }) => {
    const mode = new Animated.Value(0);
    const buttonSize = new Animated.Value(1);

    const thermometerX = mode.interpolate({
        inputRange: [0, 1],
        outputRange: [-24, -100]
    });

    const thermometerY = mode.interpolate({
        inputRange: [0, 1],
        outputRange: [-50, -100]
    });

    const timeX = mode.interpolate({
        inputRange: [0, 1],
        outputRange: [-24, -24]
    });

    const timeY = mode.interpolate({
        inputRange: [0, 1],
        outputRange: [-50, -150]
    });

    const pulseX = mode.interpolate({
        inputRange: [0, 1],
        outputRange: [-24, 50]
    });

    const pulseY = mode.interpolate({
        inputRange: [0, 1],
        outputRange: [-50, -100]
    });

    const rotation = mode.interpolate({
        inputRange: [0, 1],
        outputRange: ["0deg", "45deg"]
    });

    const sizeStyle = {
        transform: [{ scale: buttonSize }]
    };



    const handlePress = () => {
        Animated.sequence([
            Animated.timing(buttonSize, {
                toValue: 0.95,
                duration: 200,
                useNativeDriver: true,
            }),
            // Animated.timing(buttonSize, {
            //     toValue: 1,
            //     useNativeDriver: true,
            // }),
            // Animated.timing(mode, {
            //     toValue: mode._value === 0 ? 1 : 0,
            //     useNativeDriver: true,
            // })
        ]).start();
    };

    return (
        <View style={{ position: "absolute", alignItems: "center" }}>
            {/* <Animated.View style={{ position: "absolute", left: thermometerX, top: thermometerY }}>
                <View style={styles.secondaryButton}>
                    <Feather name="thermometer" size={24} color="#FFF" />
                </View>
            </Animated.View>
            <Animated.View style={{ position: "absolute", left: timeX, top: timeY }}>
                <View style={styles.secondaryButton}>
                    <Feather name="clock" size={24} color="#FFF" />
                </View>
            </Animated.View>
            <Animated.View style={{ position: "absolute", left: pulseX, top: pulseY }}>
                <View style={styles.secondaryButton}>
                    <Feather name="activity" size={24} color="#FFF" />
                </View>
            </Animated.View> */}
            <Animated.View style={[styles.button, sizeStyle]}>
                <TouchableOpacity onPress={onPress} underlayColor="#7F58FF">
                    <Animated.View style={{ transform: [{ rotate: rotation }] }}>
                        <FontAwesome5 name="plus" size={20} color="#FFF" />
                    </Animated.View>
                </TouchableOpacity>
            </Animated.View>
        </View>
    )
}


const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        justifyContent: "center",
        width: 60,
        height: 60,
        borderRadius: 30,
        marginTop: -30,
        backgroundColor: "#7F58FF",
        // position: "absolute",
        shadowColor: "#7F58FF",
        shadowRadius: 5,
        shadowOffset: { height: 10 },
        shadowOpacity: 0.3,
        borderWidth: 2,
        borderColor: "#FFFFFF"
    },
    secondaryButton: {
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
        width: 48,
        height: 48,
        borderRadius: 24,
        backgroundColor: "#7F58FF"
    }
});
export default AddButton;