import React from 'react';
import { Text, View } from 'react-native';
import { Badge } from 'native-base';

const IconWithBadge = ({ badgeCount = 0, icon}) => {
    return (
        <View style={{ width: 24, height: 24, margin: 5 }}>
            {icon}
            {badgeCount > 0 && (
                <View
                    style={{
                        // On React Native < 0.57 overflow outside of parent will not work on Android, see https://git.io/fhLJ8
                        position: 'absolute',
                        right: -6,
                        top: -3,
                        paddingLeft: 4,
                        paddingRight: 4,
                        backgroundColor: 'red',
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ color: 'white', fontSize: 10, fontWeight: 'bold'}}>
                        {badgeCount}
                    </Text>
                </View>
            )}
        </View>
    );
}

export default IconWithBadge;