import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import APP_SCREEN from '../screen';
import SCREEN_NAME from './ScreenName';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Text } from 'react-native';
import AddButton from './AddButton';
import IconWithBadge from './IconWithBadge';


const Tab = createBottomTabNavigator();

const BottomTabNavigation = () => {
    const { HOME_SCREEN_NAME, NEW_POST_SCREEN_NAME, NOTIFICATION_SCREEN_NAME, PROFILE_SCREEN_NAME, SETTING_SCREEN_NAME } = SCREEN_NAME;
    const { HOME_SCREEN, NEW_POST_SCREEN, NOTIFICATION_SCREEN, PROFILE_SCREEN, SETTING_SCREEN } = APP_SCREEN;

    const checkRoute = (route, size = 26, focused, color, navigation) => {
        const { name } = route;
        switch (name) {
            case HOME_SCREEN_NAME:
                return {
                    label: HOME_SCREEN_NAME,
                    icon: <IconWithBadge badgeCount={7} icon={
                        <MaterialCommunityIcons name={'home'} color={color} size={size} />
                    } />,
                };
            case PROFILE_SCREEN_NAME:
                return {
                    label: PROFILE_SCREEN_NAME,
                    icon: <IconWithBadge badgeCount={999} icon={
                        <MaterialCommunityIcons name={'message'} color={color} size={size} />
                    } />
                    ,
                };
            case NEW_POST_SCREEN_NAME:
                return {
                    label: '',
                    icon: <AddButton onPress={() => { navigation.navigate(NEW_POST_SCREEN_NAME)}} />,
                };
            case NOTIFICATION_SCREEN_NAME:
                return {
                    label: NOTIFICATION_SCREEN_NAME,
                    icon: <IconWithBadge badgeCount={99} icon={
                        <MaterialCommunityIcons name={'alert-circle-outline'} color={color} size={size} />
                    } />,
                };
            case SETTING_SCREEN_NAME:
                return {
                    label: SETTING_SCREEN_NAME,
                    icon: <IconWithBadge badgeCount={1} icon={
                        <MaterialCommunityIcons name={'settings-helper'} color={color} size={size} />
                    } />,
                };
            default:
                return {};
        }
    }

    return (
        <Tab.Navigator
            initialRouteName={HOME_SCREEN_NAME}
            screenOptions={({ route, navigation }) => ({
                tabBarIcon: ({ focused, size, color }) =>
                    checkRoute(route, size, focused, color, navigation).icon,
                tabBarLabel: ({ color }) => (
                    <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ color, fontSize: 10, marginBottom: 4, }}>
                        {checkRoute(route, navigation).label}
                    </Text>
                ),
            })}
            tabBarOptions={{
                activeTintColor: "#ffffff",
                inactiveTintColor: "#3e2465",
                style: {
                    backgroundColor: '#694fad'
                }
            }}

        >

            <Tab.Screen
                name={HOME_SCREEN_NAME}
                component={HOME_SCREEN}
            />


            <Tab.Screen
                name={PROFILE_SCREEN_NAME}
                component={PROFILE_SCREEN}
            />

            <Tab.Screen
                name={NEW_POST_SCREEN_NAME}
                component={NEW_POST_SCREEN}
            />

            <Tab.Screen
                name={NOTIFICATION_SCREEN_NAME}
                component={NOTIFICATION_SCREEN}
            />

            <Tab.Screen
                name={SETTING_SCREEN_NAME}
                component={SETTING_SCREEN}
            />
        </Tab.Navigator>
    );
}

export default BottomTabNavigation;