import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { ACTION_GET_PROFILE } from '../../../redux/profile/actions';
import MyHeader from '../../../components/Header';
import i18n from '../../../i18n'

const HomeScreen = () => {
    const dispatch = useDispatch();

    // const getProfile = () => { dispatch(ACTION_GET_PROFILE()) };
    const [lang, setLang] = useState('en-US')

    function changeLanguage () {
        setLang('vi-VN');
        i18n.locale = 'vi-VN'
    }

    const state = useSelector(state => state);

    useEffect(() => {

    })

    return (
        <>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>{`${JSON.stringify(state)}`}</Text>
                <TouchableOpacity
                    onPress={changeLanguage}
                    style={{ borderWidth: 1, borderColor: 'red', padding: 10, borderRadius: 5, }}
                >
                    <Text>{i18n.t('BottomNavigationTab.home')}</Text>
                </TouchableOpacity>
            </View>
        </>

    );
}

export default HomeScreen;
