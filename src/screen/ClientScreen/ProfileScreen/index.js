import * as React from 'react';
import { Text, View } from 'react-native';
import { Dimensions, SafeAreaView, TouchableOpacity, ScrollView, FlatList } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


// const DATA = [{ id: 1, title: "Item One" }, { id: 2, title: "Item Two" }, { id: 3, title: "Item Three" }, { id: 4, title: "Item Four" }, { id: 5, title: "Item Five" }, { id: 6, title: "Item Six" }, { id: 7, title: "Item Seven" }, { id: 8, title: "Item Eight" }, { id: 9, title: "Item Nine" }, { id: 10, title: "Item Ten" }, { id: 11, title: "Item eleven" }, { id: 12, title: "Item Twelve" }, { id: 13, title: "Item Thirteen" }];
// const WIDTH = Dimensions.get('screen').width; 

const ProfileScreen = () => {

    const [columnCount, setColumnCount] = React.useState(2);
    const homeMenus = [
        { content: 'Content', colNumber: 1 },
        { content: 'Content1', colNumber: 2 },
        { content: 'Content2', colNumber: 2 },
        { content: 'Content3', colNumber: 2 },
        { content: 'Content4', colNumber: 2 },
        { content: 'Content5', colNumber: 3 },
        { content: 'Content7', colNumber: 3 },
        { content: 'Content6', colNumber: 3 },
        { content: 'Content8', colNumber: 4 },
        { content: 'Content9', colNumber: 4 },
        { content: 'Content10', colNumber: 4 },
        { content: 'Content', colNumber: 4 },
    ];

    // const renderItem = (item, index) => {
    //     console.log('item', item);
    //     if (item[0].type == "banner") {
    //         return (
    //             <View key={index} style={{ width: WIDTH - 20, flexDirection: 'row', marginLeft: 10, marginRight: 10, marginBottom: 10, height: 60, backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center' }}>
    //                 <Text style={{ textAlign: 'center', color: 'white' }}> YOUR AD BANNER COMPONENT CAN BE PLACED HERE HERE </Text>
    //             </View>
    //         )
    //     }
    //     const columns = item.map((val, idx) => {
    //         return (
    //             <View style={{ width: WIDTH / 3 - 20, justifyContent: 'center', backgroundColor: 'gray', height: 60, marginLeft: 10, marginRight: 10 }} key={idx}>
    //                 <Text style={{ textAlign: 'center' }}> {val.title} </Text>
    //             </View>
    //         )
    //     });
    //     return (
    //         <View key={index} style={{ width: WIDTH, flexDirection: 'row', marginBottom: 10 }}>
    //             {columns}
    //         </View>
    //     )
    // }
    // const modifyData = (data) => {
    //     const numColumns = 3;
    //     const addBannerIndex = 6;
    //     const arr = [];
    //     var tmp = [];
    //     data.forEach((val, index) => {
    //         if (index % numColumns == 0 && index != 0) {
    //             arr.push(tmp);
    //             tmp = [];
    //         }
    //         if (index % addBannerIndex == 0 && index != 0) {
    //             arr.push([{ type: 'banner' }]);
    //             tmp = [];
    //         }
    //         tmp.push(val);

    //     });
    //     arr.push(tmp);
    //     return arr;
    // }
    // const newData = modifyData(DATA);
    return (
        <SafeAreaView style={{ flex: 1, }}>
            {/* <FlatList
                data={newData}
                renderItem={({ item, index }) => renderItem(item, index)}
            />  */}
            <ScrollView>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {
                        homeMenus.map((value, index) => {
                            const { content, colNumber } = value
                            const ratio = 12;
                            const widthHeightItem = ((windowWidth) / ratio);
                            const widthItem = widthHeightItem * (ratio / colNumber) ;
                            return (
                                <HomeMenuItem key={`${index}`} widthItem={widthItem} heightItem={widthHeightItem * 4} item={content} />
                            )
                        })
                    }
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export const HomeMenuItem = ({ widthItem, heightItem, item }) => {

    return (
        <View style={{
            width: widthItem,
            height: heightItem,
            backgroundColor: 'transparent',
        }}
        >
            <TouchableOpacity
                style={{
                    width: widthItem - 12,
                    height: heightItem - 12,
                    alignSelf: 'center',
                    backgroundColor: 'red',
                    justifyContent: 'center',
                    borderRadius: 3,
                }}
            >
                <Text style={{ color: 'white', alignSelf: 'center' }}>{`${JSON.stringify(item)}`}</Text>
            </TouchableOpacity>
        </View>

    )
}

export default ProfileScreen;
