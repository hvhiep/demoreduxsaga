import * as React from 'react';
import { Text, View } from 'react-native';
import i18n from '../../../i18n'

const NewPostScreen = () => {
    
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>{i18n.t('BottomNavigationTab.home')}</Text>
        </View>
    );
}

export default NewPostScreen;
